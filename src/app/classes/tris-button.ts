export class TrisButton {

    private _value: boolean;
    public get value() { return this._value; }
    public set value(value: boolean) { this._value = value; }

    public get disabled() {
        return this.value !== undefined;
    }

    constructor() { }

    public reset() {
        this.value = undefined;
    }
}