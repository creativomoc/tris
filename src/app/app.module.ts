import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '@angular/material';
import { AppService } from './app.service';
import { AppComponent } from './app.component';
import { ResultDialogComponent } from './dialogs/result-dialog.component';

import 'hammerjs';

@NgModule({
  declarations: [
    AppComponent,
    ResultDialogComponent
  ],
  entryComponents: [
    ResultDialogComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule
  ],
  providers: [
    AppService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
