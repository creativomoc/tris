import { Component, OnInit } from '@angular/core';
import { MdDialogRef } from '@angular/material';

@Component({
    moduleId: module.id,
    selector: 'tris-result-dialog',
    templateUrl: 'result-dialog.component.html'
})

export class ResultDialogComponent implements OnInit {

    public get resultText() { 
        switch(this.result){
            case 1:
                return 'You win!';
            case 2:
                return 'You lose!';
            case 3:
                return 'Match draw!';
        }
     }

    private _result: number;
    public get result() { return this._result; }
    public set result(value:number) { this._result = value; }

    constructor(public dialogRef: MdDialogRef<ResultDialogComponent>) {}

    ngOnInit() { }
}