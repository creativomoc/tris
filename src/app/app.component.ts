import { Component } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  get buttons() { return this.appService.buttons; }

  public click(button) {
    this.appService.play(button);
  }

  public reset() {
    this.appService.reset();
  }

  constructor(private appService: AppService){ }
}
