import { Injectable } from '@angular/core';
import { TrisButton } from './classes/tris-button';
import { MdDialog } from '@angular/material';
import { ResultDialogComponent } from './dialogs/result-dialog.component';

@Injectable()
export class AppService {

    private _buttons: TrisButton[];
    public get buttons() { return this._buttons; }
    public set buttons(value: TrisButton[]) { this._buttons = value; }

    private whoBegins: boolean = Math.floor(Math.random() * 2) === 0;

    constructor(public dialog: MdDialog) {
        this.buttons = [];
        for (let index = 0; index < 9; index++) {
            this.buttons.push(new TrisButton());
        }

        if (this.whoBegins) {
            this.computer();
        }
    }

    public play(button: TrisButton) {

        // button already clicked
        if (button.value !== undefined) {
            return;
        }

        let checkValue;

        // user
        button.value = true;
        checkValue = this.check();
        if (checkValue > 0) {
            this.end(checkValue);
            return;
        }

        // computer
        this.computer();
        checkValue = this.check();
        if (checkValue > 0) {
            this.end(checkValue);
            return;
        }
    }

    /**
     * return:
     *  0 if match not yet finished
     *  1 if user won
     *  2 if computer won
     *  3 if match draw
     */
    private check(): number {
        let index;

        // horizontal
        for (index = 0; index < 9; index += 3) {
            if (
                this.buttons[index].value !== undefined
                && this.buttons[index].value === this.buttons[index + 1].value
                && this.buttons[index].value === this.buttons[index + 2].value
            ) {
                return this.buttons[index].value ? 1 : 2;
            }
        }

        // vertical
        for (index = 0; index < 3; index++) {
            if (
                this.buttons[index].value != undefined
                && this.buttons[index].value === this.buttons[index + 3].value
                && this.buttons[index].value === this.buttons[index + 6].value
            ) {
                return this.buttons[index].value ? 1 : 2;
            }
        }

        // diagonal
        if (
            this.buttons[0].value != undefined
            && this.buttons[0].value === this.buttons[4].value
            && this.buttons[0].value === this.buttons[8].value
        ) {
            return this.buttons[0].value ? 1 : 2;
        }
        if (
            this.buttons[2].value != undefined
            && this.buttons[2].value === this.buttons[4].value
            && this.buttons[2].value === this.buttons[6].value
        ) {
            return this.buttons[2].value ? 1 : 2;
        }

        // match not yet finished
        for (index = 0; index < 9; index++) {
            if (this.buttons[index].value === undefined) {
                return 0;
            }
        }

        // draw
        return 3;
    }

    private computer() {
        let index;
        let max = -100000, mi = 4, t;
        for (index = 0; index < 9; index++) {
            if (this.buttons[index].value === undefined) {
                this.buttons[index].value = false;
                t = this.alphabeta(true, 20, -100000, 100000, true);
                if (t > max) {
                    max = t;
                    mi = index;
                }
                this.buttons[index].value = undefined;
            }
        }
        this.buttons[mi].value = false;
    }


    private alphabeta(player, depth, alpha, beta, maximizes): number {

        const checkValue = this.check();
        switch (checkValue) {
            case 2:
                return (99980 + depth);
            case 1:
                return (-99980 - depth);
            case 3:
                return 0;
        }

        let result;
        if (player) {
            result = 100000;
            for (let index = 0; index < 9; index++) {
                if (this.buttons[index].value === undefined) {

                    this.buttons[index].value = player;
                    result = Math.min(result, this.alphabeta(false, depth - 1, alpha, beta, true));
                    beta = Math.min(beta, result);
                    this.buttons[index].value = undefined;
                    if (beta <= alpha) {
                        break;
                    }

                }
            }
        }
        else {
            result = -100000;
            for (let index = 0; index < 9; index++) {
                if (this.buttons[index].value === undefined) {

                    this.buttons[index].value = player;
                    result = Math.max(result, this.alphabeta(true, depth - 1, alpha, beta, false));
                    alpha = Math.max(alpha, result);
                    this.buttons[index].value = undefined;
                    if (beta <= alpha) {
                        break;
                    }
                }
            }
        }

        return result;
    }

    private end(mod) {
        const dialogRef = this.dialog.open(ResultDialogComponent);
        dialogRef.componentInstance.result = mod;
        dialogRef.afterClosed().subscribe(() => {
            this.reset();
        });
    }

    public reset() {
        for (let index = 0; index < 9; index++) {
            this.buttons[index].value = undefined;
        }
        this.whoBegins = !this.whoBegins;
        if (this.whoBegins) {
            this.computer();
        }
    }
}